const VIRTUAL_HOST = 'https://uenv.ru';
const CLOUD_LOGIN = 'https://deploy.uenv.ru/login/yandex_cloud/'

module.exports = {
    title: 'UniEnv — Помощь',
    description: 'UniEnv — Помощь',
    dest: './static/help/',
    base: '/help/',
    sourceDir: 'help/',
    devServer: {
        writeToDisk: true
    },
    themeConfig: {
        lastUpdated: 'Обновлено',
        docsRepo: 'https://gitlab.com/unienv/help',
        editLinks: true,
        editLinkText: 'Нашли ошибку? Предложите исправление!',
        sidebarDepth: 3,
        sidebar: {
            '/cloud/': [
                '/cloud/',
                {
                    title: 'Serverless',
                    collapsable: false,
                    path: '/cloud/serverless/',
                    children: [
                        '/cloud/serverless/build',
                        '/cloud/serverless/registry',
                        '/cloud/serverless/container',
                        '/cloud/serverless/api_gateway',
                        '/cloud/serverless/problems',
                    ]
                },
                '/cloud/compute-cloud',
            ],
            '/unienv_deploy/': [
                '/unienv_deploy/',
                '/unienv_deploy/fixing',
                '/unienv_deploy/from-image'
            ],
            '/': [
                {
                    title: 'Подключение авторизации',
                    collapsable: false,
                    path: '/auth'
                },
                {
                    title: 'VPN gateway',
                    collapsable: false,
                    path: '/vpn-gateway'
                },

                {
                    title: 'Облачное развертывание',
                    collapsable: false,
                    path: '/cloud/'
                },
                {
                    title: 'Развертывание',
                    collapsable: false,
                    children: [
                        'deploy/unienv_db',
                        'deploy/docker',
                        'deploy/env', 
                        'deploy/gitlab-ci',
                        'deploy/ssh',
                        'deploy/manual-git',
                        'deploy/kpfu'
                    ]
                },
                {
                    title: 'О проекте',
                    collapsable: false,
                    path: '',
                    children: [
                        'monitoring',
                        'changelog',
                        'tech',
                        '/unienv_deploy/',
                        { title: "При поддержке лаборатории Smart Education Lab КФУ ИТИС", type: 'external', path: 'https://kpfu.ru/itis' },
                    ]
                },
            ],
            
        },
        nav: [
            { text: "Главная", link: '/' },
            { text: "Облачное развертывание", link: '/cloud/' },
            { text: "Вход в UniEnv", link: VIRTUAL_HOST },
            { text: "Вход Я.Облако", link: CLOUD_LOGIN },
        ],
    },
    markdown: {
        lineNumbers: true
    },
    extendPageData($page) {
        $page.VIRTUAL_HOST = 'https://uenv.ru';
        $page.CORE_URL = 'https://uenv-core.kpfu.ru/';
        $page.DOCS_SWAGGER_URL = `${$page.CORE_URL}/docs/swagger_ui/`;
        $page.DOCS_REDOC_URL = `${$page.CORE_URL}/docs/redoc/`;
        $page.SUPPORT_LINK = 'https://t.me/atnartur';
        $page.CLOUD_LOGIN = CLOUD_LOGIN;
        $page.YANDEX_CLI_HELP = 'https://deploy.uenv.ru/help/yandex_cli/';
    }
};
