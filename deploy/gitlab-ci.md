# Развертывание с помощью Gitlab CI



## Добавление Gitlab Runner на сервер

1. Откройте репозиторий
2. Выберите Settings / CI/CD / Runners
3. Скопируйте `registration token`
4. Выполните команду для запуска Gitlab Runner на сервере:

```bash{8,10}
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.gitlab-runner:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest

docker exec -ti gitlab-runner gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token TOKEN \  # впишите токен
  --executor docker \
  --description "project name" \
  --docker-image "atnartur/docker:latest" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

docker exec -ti gitlab-runner gitlab-runner start
```

5. Обновите страницу настроек репозитория в Gitlab. Gitlab runner должен появиться там.
6. Нажмите на иконку редактирования и добавьте тег с названием проекта латиницей, он пригодится нам в дальнейшем

## Конфигурация Gitlab CI

Добавьте файл `.gitlab-ci.yml` (с точкой в начале) следующего содержимого и поправьте выделенные строчки

```yaml{13,15,25,27}
image: atnartur/docker:latest

stages:
  - build
  - deploy

# сборка проекта
build:
  stage: build
  script:
    - docker-compose -f docker-compose.prod.yml build
  tags:
    - projectname  # укажите тег вашего раннера
  only:
  	- develop  # укажите ветку, с которой будет происходить развертывание проекта

# запуск проекта
deploy_linux:
  stage: deploy
  script:
    - docker-compose -f docker-compose.prod.yml stop
    - docker-compose -f docker-compose.prod.yml rm -f
    - docker-compose -f docker-compose.prod.yml up -d
  tags:
    - projectname  # укажите тег вашего раннера
  only:
  	- develop  # укажите ветку, с которой будет происходить развертывание проекта
```

Сделайте коммит с этими изменениями и проверьте логи в разделе Pipelines.

## Больше информации

- [Статья от Доки про Gitlab CI](https://doka.guide/tools/gitlab-ci-cd/)
- [Официальная документация по файлу .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)
