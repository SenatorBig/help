# Работа с GIT репозиторием без CI

## Настройка Git, для работы с GitLab без CI

```
sudo apt install git -y

git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "MY_NAME@example.com"

ssh-keygen -t ed25519 -C "name.surname"

/home/rasimusv/.ssh/id_ed25519_gitlab

cat ~/.ssh/id_ed25519_gitlab.pub

https://gitlab.com/-/profile/keys
```

## Установка необходимых пакетов

```
cd /opt/

sudo git clone repo-link

cd repo-dir/

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" -y

sudo apt update -y

sudo apt install docker-ce docker-compose -y

sudo usermod -a -G docker $USER
```

## Сборка и деплой приложения в Docker

```
git clone https://github.com/rasimusv/examjava2022.git

sudo COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build
```