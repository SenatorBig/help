---
prev: false
next: false
---

# КФУ VPN gateway

::: tip
Open VPN шлюз для подключения к КФУ VPN
:::

- **Что это?** VPN сервер на технологии [Open VPN](https://openvpn.net/) для подключения к сервисам КФУ, защищенный авторизацией через университетский аккаунт.
- **Для кого?** Преимущественно для пользователей Mac OS.
- **Зачем?** [Apple прекратила поддержку PPTP VPN, который развернут в КФУ, из-за проблем с безопаностью](https://support.apple.com/en-gb/HT206844). Теперь подключиться к таким VPN-серверам с Mac OS невозможно с помощью системных и бесплатных способов.

## Подключение

::: warning
**Мощности этого сервера ограничены**: просьба подключаться к VPN только для использования корпоративных сервисов КФУ и отключаться сразу после этого.
:::

**Инструкция предназначена для пользователей Mac OS**. Пользователи других операционных систем тоже могут воспользоваться этим сервером, но лучше использовать [официальные способы](https://kpfu.ru/main_page?p_cid=71473&p_random=276). 

1. Скачать [Tunnelblick](https://tunnelblick.net/downloads.html)
2. Скачать [конфигурационный файл Open VPN](https://yadi.sk/d/59LqqA5q3TsrQA)
3. Открыть этот файл в текстовом редакторе
4. Добавить строчку `auth-user-pass /Users/USERNAME/Documents/kpfu.txt`, где `USERNAME` - имя пользователя на компьютере
5. В папке "Документы" создать файл `kpfu.txt` со следующим содержимым:

```
IIIvanov
PASSWORD
```

`IIIvanov` - логин от личного кабинета КФУ, `PASSWORD` - пароль от него.

6. Перетащить конфигурационный файл Open VPN в Tunnelblick
7. Подключиться к VPN
8. Готово!

## Использование Gitlab Высшей школы ИТИС

### Вход 

1. После подключения к VPN открыть Gitlab по адресу [10.13.1.54](http://10.13.1.54)
2. Авторизоваться с помощью электронной почты (например, `IIIvanov@stud.kpfu.ru`) и пароля. Эти же данные надо будет ввести при выполнении `git push` или `git pull`.

### Подключение к репозиториям

Для работы c репозиториями нужно заменить адрес `gititis.kpfu.ru` на `10.13.1.54`.

Примеры команд:

- `git clone http://10.13.1.54/username/reponame.git` - клонирование нового репозитория
- `git remote set-url origin http://10.13.1.54/username/reponame.git` - изменение удаленного адреса для существующего репозитория
- `git remote add itis http://10.13.1.54/username/reponame.git` - добавление нового remote

`username` - имя пользователя или группы в Gitlab, `reponame` - название репозитория.

[Подробнее о git remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes).

## Используемые инструменты

- [Официальная инструкция по подключению к КФУ VPN](https://kpfu.ru/ictis/chasto-zadavaemye-voprosy/kak-nastroit-vpn-soedinenie-71473.html)
- [PPTP конфиг для MacOS](https://gist.github.com/KostyaEsmukov/b19ac06afbd68f391262a11d6a712750) - уже не работает
- [PPTP VPN ON MAC OS X SIERRA, HIGH SIERRA AND MOJAVE FOR FREE](https://filipmolcik.com/pptp-vpn-on-macos-sierra-and-high-sierra/) - тоже не сработал
- [kylemanna/docker-openvpn - OpenVPN server in Docker](https://github.com/kylemanna/docker-openvpn)
- [Using alternative authentication methods - OpenVPN](https://openvpn.net/community-resources/using-alternative-authentication-methods/)
- [Авторизация на Университетской платформе](/auth.html)

