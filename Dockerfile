FROM node:16.15.0-alpine

RUN apk add --no-cache git

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY . .

RUN npm run build