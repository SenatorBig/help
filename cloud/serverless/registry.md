# Отправка Docker-образов в Cloud Registry

Yandex Cloud Registry – сервис для хранения Docker-образов. На основе этих образов можно будет запустить контейнер в облаке. [Подробнее о сервисе в официальной документации](https://cloud.yandex.ru/docs/container-registry/).

## Создание Registry

У каждого приложения должен быть свой Registry. Для создания

1. <a :href="$page.CLOUD_LOGIN" target="_blank">Перейдите в консоль</a>
2. Выберите сервис **Container registry**
3. Нажмите на кнопку **Создать реестр**
4. Впишите название (например, `main`)
5. В списке появится registry, он пригодится нам позже

![](../../images/cloud/serverless/registry.png)

**Создадим политику автоудаления**. Она поможет не хранить нам лишние образы в registry.
1. Откройте registry
2. В левом меню перейдите в раздел "Жизненный цикл"
3. Нажмите на кнопку "Создать политику удаления"
4. Заполните настройки
   1.  Имя: `autodelete`
   2.  Статус: `ACTIVE`
   3.  Тег: `.*`
   4.  Образы без тегов: `да`
   5.  Время от создания образа, в сутках: `1`
5. Внизу нажмите на кнопку "Создать"

## Авторизуйтесь в Registry

1. <a :href="$page.YANDEX_CLI_HELP" target="_blank">Установите Yandex Cloud CLI</a>
2. Выполните: 
```bash
yc container registry configure-docker
```

## Сборка и отправка образа

Соберите образ со вставкой ID Вашего registry и отправьте его в registry
```bash
docker build -t cr.yandex/{registry_id}/app:latest .
docker push cr.yandex/{registry_id}/app:latest
```

После этого образ должен отобразиться в интерфейсе

![](../../images/cloud/serverless/registry_image.png)

Теперь образ можно использовать для развертывания.