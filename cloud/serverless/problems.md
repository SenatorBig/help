---
next: false
---

# Решение проблем

В этой статье описаны некоторые возможные проблемы и пути их решения

::: tip Не забывайте про логи!
Их можно прочесть в сервисе **Cloud Logging**. Выбирайте временной интервал и читайте логи всех сервисов в одном месте.
:::


## Container ... not found
```json
{
    "errorCode":404,
    "errorMessage":"Not Found: request-id = ... rpc error: code = NotFound desc = Container ... not found",
    "errorType":"ClientError"
}
```

Проверьте CONTAINER_ID в конфигурации API Gateway, возможно, он указан неправильно.

## Container exited...

```json
{
    "errorMessage":"user container finished with error: exit status 2",
    "errorType":"UserCodeError"
}
```

Проблема с запуском контейнера. Для получения подробностей читайте логи контейнера.