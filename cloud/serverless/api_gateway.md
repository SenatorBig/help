# Создание API Gateway

API Gateway позволяет объединить несколько компонентов одного приложения под одним адресом. [Подробнее о сервисе](https://cloud.yandex.ru/docs/api-gateway/).

Для того, чтобы создать API Gateway, нужно собрать информацию, подготовить конфигурацию и сохранить ее в системе.

## Сбор информации

1. <a :href="$page.CLOUD_LOGIN" target="_blank">Перейдите в консоль</a>
2. Получение SERVICE_ACCOUNT_ID
   1. Нажмите сверху на вкладку **Права доступа**
   2. Скопируйте идентификатор сервисного аккаунта `student`
   3. Вернитесь на главную страницу
3. Получение CONTAINER_ID
   1. Зайдите в **Serverless containers**
   2. Скопируйте идентификатор контейнера

## Подготовка конфигурации

Ниже приведен пример конфигурации. Возьмите его, замените название проекта, ID контейнера и сервисного аккаунта

```yml{4,18,19}
openapi: "3.0.0"
info:
  version: 1.0.0
  title: PROJECT NAME # название проекта
paths:
  /{url+}:
    x-yc-apigateway-any-method:
      summary: Execute container
      operationId: container
      parameters:
      - explode: false
        in: path
        name: url
        required: false
        style: simple
      x-yc-apigateway-integration:
        type: serverless_containers
        container_id: CONTAINER_ID # ID контейнера
        service_account_id: SERVICE_ACCOUNT_ID # ID сервисного аккаунта
```

## Создание API Gateway

1. Зайдите в **API Gateways**
2. Нажмите **Создать API шлюз**
3. Укажите название
4. Вставьте конфигурацию, которая была подготовлена ранее
5. Остальные параметры оставьте без изменений
6. Нажмите **Создать**

Когда статус шлюза станет **Active** внутри появится ссылка вида `https://....apigw.yandexcloud.net`, по которой можно будет перейти и увидеть приложение. Приложение развернуто!